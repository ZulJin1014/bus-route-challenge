package com.goeuro.dev;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedMap;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;
import java.util.stream.IntStream;

public class StationMatrixBuilder
{

  public static final int MAX_VALUE_OF_STATIONS = 1_000_000;

  public ImmutableList<ImmutableSortedMap<Integer, Integer>> build( List<String> busRouteData )
  {
    ArrayList<SortedMap<Integer, Integer>> allRoutesByStationId = new ArrayList<>();

    for( int i = 0; i < MAX_VALUE_OF_STATIONS; i++ ) {
      allRoutesByStationId.add( new TreeMap<>() );
    }

    busRouteData.forEach( line -> {
      String[] elements = line.split( " " );
      Integer route = Integer.parseInt( elements[0] );
      IntStream.range( 1, elements.length )
        .forEach( stationIndex -> allRoutesByStationId.get(
          Integer.parseInt( elements[stationIndex] ) )
          .put( route, stationIndex ) );
    } );

    return allRoutesByStationId.stream()
      .map( ImmutableSortedMap::copyOf )
      .collect( collectingAndThen( toList(), ImmutableList::copyOf ) );
  }
}
