package com.goeuro.dev;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedMap;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DataService
  implements InitializingBean
{

  @Value("${filePath}")
  String filePath;
  public static final int MAX_VALUE_OF_STATIONS = 1_000_000;
  ImmutableList<ImmutableSortedMap<Integer, Integer>> preparedData;

  @Override
  public void afterPropertiesSet()
    throws Exception
  {
    this.preparedData = prepareData();
  }

  public Boolean isDirectBusRouteExist( Integer depSid, Integer arrSid )
    throws Exception
  {

    if (depSid < 0 && depSid >= MAX_VALUE_OF_STATIONS || arrSid < 0 && arrSid >= MAX_VALUE_OF_STATIONS) {
      throw new Exception( "property(ies) are too big" );
    }

    ImmutableList<Integer> depKeyList = preparedData.get( depSid ).navigableKeySet().asList();
    ImmutableList<Integer> arrKeyList = preparedData.get( arrSid ).navigableKeySet().asList();
    int depIterator = 0;
    int arrIterator = 0;
    while ( depIterator < depKeyList.size() && arrIterator < arrKeyList.size() ) {

      Integer cuurentDepKeyValue = depKeyList.get( depIterator );
      Integer currentArrKeyValue = arrKeyList.get( arrIterator );

      if ( cuurentDepKeyValue > currentArrKeyValue ) {
        arrIterator++;
      } else if ( cuurentDepKeyValue < currentArrKeyValue ) {
        depIterator++;
      } else {
        if ( cuurentDepKeyValue.equals( currentArrKeyValue ) &&
          preparedData.get( depSid ).get( cuurentDepKeyValue ) <=
            preparedData.get( arrSid ).get( currentArrKeyValue ) ) {
          return true;
        } else {
          depIterator++;
        }
      }
    }
    return false;
  }

  private ImmutableList<ImmutableSortedMap<Integer, Integer>> prepareData()
    throws IOException
  {
    List<String> routes = Files.readAllLines( Paths.get( filePath ) );
    StationMatrixBuilder stationMatrixBuilder = new StationMatrixBuilder();
    return stationMatrixBuilder.build( routes );
  }
}
