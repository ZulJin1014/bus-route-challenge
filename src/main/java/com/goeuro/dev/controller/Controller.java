package com.goeuro.dev.controller;

import com.goeuro.dev.DataService;
import com.goeuro.dev.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller
{
  @Autowired
  private DataService dataService;

  @RequestMapping(value = "/api/direct", method = RequestMethod.GET)
  public
  @ResponseBody
  Response checkDirectBusRoute( @RequestParam(value = "dep_sid") Integer depSid,
    @RequestParam("arr_sid") Integer arrSid )
    throws Exception
  {
    if ( depSid.equals( arrSid ) ) {
      return new Response( true, depSid, arrSid );
    }
    Boolean answer = dataService.isDirectBusRouteExist( depSid, arrSid );
    return new Response( answer, depSid, arrSid );
  }
}
