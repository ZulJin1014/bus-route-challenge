package com.goeuro.dev;

import com.goeuro.dev.controller.Controller;
import com.goeuro.dev.model.Response;
import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(Controller.class)
public class ControllerTest
{
  public static final String VALID_URL = "/api/direct?dep_sid=3&arr_sid=6";
  public static final String URL_WITH_INVALID_PROPERTY = "/api/direct?dep_sid=TEST&arr_sid=6";
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private DataService dataService;

  @Test
  public void checkRest()
    throws Exception
  {
    given( dataService.isDirectBusRouteExist( 3, 6 ) )
      .willReturn( true );
    mockMvc.perform( get( VALID_URL ).accept( MediaType.ALL ) )
      .andExpect( status().isOk() )
      .andExpect( content().contentType( MediaType.APPLICATION_JSON_UTF8 ) )
      .andExpect( content().string( new Gson().toJson( new Response( true, 3, 6 ) ) ) );
  }

  @Test
  public void sendNotNumberProperty()
    throws Exception
  {
    mockMvc.perform( get( URL_WITH_INVALID_PROPERTY ) )
      .andExpect( status().is4xxClientError() );
  }

  @Test
  public void sendEqualsRoutes()
    throws Exception
  {
    mockMvc.perform( get( "/api/direct?dep_sid=6&arr_sid=6" ) )
      .andExpect( status().isOk() );
  }
}
