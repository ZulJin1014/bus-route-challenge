package com.goeuro.dev;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class DataServiceTest
{
  public static DataService dataService;
  @BeforeClass
  public static void init()
    throws Exception
  {
    dataService = new DataService();
    dataService.filePath="./src/test/resources/testData.txt";
    dataService.afterPropertiesSet();
  }
  @Test
  public void readFromFileTest()
    throws Exception
  {
    Boolean answer = dataService.isDirectBusRouteExist( 153,114 );
    Assert.assertEquals( answer,true );
  }

  @Test
  public void sendNonExistentStations()
    throws Exception
  {
    Boolean answer = dataService.isDirectBusRouteExist( 999_999,100_000 );
    Assert.assertEquals( answer,false );
  }

  @Test(expected = Exception.class)
  public void sendNegativeId()
    throws Exception
  {
    dataService.isDirectBusRouteExist( -1,-1 );
  }

  @Test(expected = Exception.class)
  public void sendHugeStations()
    throws Exception
  {
    dataService.isDirectBusRouteExist( 1,1_000_000 );
  }
}
